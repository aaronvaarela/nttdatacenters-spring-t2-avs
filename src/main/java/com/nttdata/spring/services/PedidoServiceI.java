package com.nttdata.spring.services;

import com.nttdata.spring.respository.Pedido;
import com.nttdata.spring.respository.Producto;

public interface PedidoServiceI {
	/**
	 * Calcular el PVP del producto seleccionado en función de si al pedido le recoresponde añadir el IVA o el IPSI
	 * @param pedido {@link Pedido} pedido
	 * @param producto {@link Producto} producto
	 * @throws Exception En caso de error
	 * @return {@link Producto} con PVP calculado
	 */
	public Producto calcularPvpProducto(Pedido pedido, Producto producto) throws Exception;
	
	/**
	 * Agregar un producto a un determinado pedido
	 * @param pedido {@link Pedido} pedido
	 * @param producto {@link Producto} producto
	 * @throws Exception En caso de error
	 */
	public void agregarProducto(Pedido pedido, Producto producto) throws Exception;
}
