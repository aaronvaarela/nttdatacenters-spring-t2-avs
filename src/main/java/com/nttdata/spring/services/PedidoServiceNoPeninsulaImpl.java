package com.nttdata.spring.services;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.nttdata.spring.respository.Pedido;
import com.nttdata.spring.respository.Producto;

@Service
@Qualifier(value = "noPeninsula")
public class PedidoServiceNoPeninsulaImpl implements PedidoServiceI {

	final int IPSI = 4;
	
	@Override
	public Producto calcularPvpProducto(Pedido pedido, Producto producto) {
		
		float precioBase = producto.getPrecioSinImpuestos();
		producto.setPvp(precioBase + precioBase * IPSI / 100);
		
		return producto;
	}

	@Override
	public void agregarProducto(Pedido pedido, Producto producto) {
		pedido.addProducto(producto);
	}

}
