package com.nttdata.spring.services;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.nttdata.spring.respository.Pedido;
import com.nttdata.spring.respository.Producto;

@Service
@Qualifier(value = "peninsula")
public class PedidoServicePeninsulaImpl implements PedidoServiceI {

	final int IVA = 21;
	
	@Override
	public Producto calcularPvpProducto(Pedido pedido, Producto producto) {
		
		float precioBase = producto.getPrecioSinImpuestos();
		producto.setPvp(precioBase + precioBase * IVA / 100);
		
		return producto;
	}

	@Override
	public void agregarProducto(Pedido pedido, Producto producto) {
		pedido.addProducto(producto);
	}

}
