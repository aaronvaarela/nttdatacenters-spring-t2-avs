package com.nttdata.spring.respository;

public class Producto {
	
	int ind;

	String nombre;

	float pvp;

	float precioSinImpuestos;

	public Producto() {
		super();
	}

	public int getInd() {
		return ind;
	}

	public void setInd(int ind) {
		this.ind = ind;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPvp() {
		return pvp;
	}

	public void setPvp(float pvp) {
		this.pvp = pvp;
	}

	public float getPrecioSinImpuestos() {
		return precioSinImpuestos;
	}

	public void setPrecioSinImpuestos(float precioSinImpuestos) {
		this.precioSinImpuestos = precioSinImpuestos;
	}

	@Override
	public String toString() {
		return "Producto " + ind + " -> nombre: " + nombre + "; precio sin impuestos: "
				+ precioSinImpuestos + "€";
	}
	
}
