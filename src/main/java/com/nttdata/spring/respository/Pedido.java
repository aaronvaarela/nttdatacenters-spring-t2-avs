package com.nttdata.spring.respository;

import java.util.ArrayList;
import java.util.List;

public class Pedido {

	int ind;
	
	List<Producto> productos;
	
	String destinatario;

	String direccion;

	boolean peninsular;
	
	public Pedido() {
		super();
		this.productos= new ArrayList<>(); 
	}
	
	public List<Producto> addProducto(Producto producto) {
		this.productos.add(producto);
		return this.productos;
	}

	public int getInd() {
		return ind;
	}

	public void setInd(int ind) {
		this.ind = ind;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public boolean isPeninsular() {
		return peninsular;
	}

	public void setPeninsular(boolean peninsular) {
		this.peninsular = peninsular;
	}

	@Override
	public String toString() {
		return "Pedido " + ind + " -> destinatario: " + destinatario + "; direccion: "
				+ direccion;
	}
	
}
