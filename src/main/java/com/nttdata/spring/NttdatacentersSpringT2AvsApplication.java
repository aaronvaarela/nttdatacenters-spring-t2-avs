package com.nttdata.spring;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.nttdata.spring.respository.Pedido;
import com.nttdata.spring.respository.Producto;
import com.nttdata.spring.services.PedidoServiceI;

@SpringBootApplication
public class NttdatacentersSpringT2AvsApplication implements CommandLineRunner {

	@Autowired
	@Qualifier(value = "peninsula")
	private PedidoServiceI pedidoPeninsulaService;

	@Autowired
	@Qualifier(value = "noPeninsula")
	private PedidoServiceI pedidoNoPeninsulaService;

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(NttdatacentersSpringT2AvsApplication.class, args);
		// ctx.close();
	}

	@Override
	public void run(String... args) throws Exception {
		List<Producto> listaProductos = new ArrayList<>();
		List<Pedido> listaPedidos = new ArrayList<>();

		Producto producto1 = new Producto();
		producto1.setInd(1);
		producto1.setNombre("ASUS TUF Dash F15");
		producto1.setPrecioSinImpuestos(800.0f);

		Producto producto2 = new Producto();
		producto2.setInd(2);
		producto2.setNombre("Samsung Galaxy M13");
		producto2.setPrecioSinImpuestos(200.0f);

		Producto producto3 = new Producto();
		producto3.setInd(3);
		producto3.setNombre("Newskill Drakain Ivory");
		producto3.setPrecioSinImpuestos(35.0f);

		listaProductos.add(producto1);
		listaProductos.add(producto2);
		listaProductos.add(producto3);

		Pedido pedido1 = new Pedido();
		pedido1.setInd(1);
		pedido1.setDestinatario("Aaron Varela");
		pedido1.setDireccion("C/ de Tabernillas, 15");
		pedido1.setPeninsular(true);
		pedidoPeninsulaService.agregarProducto(pedido1, producto1);
		pedidoPeninsulaService.agregarProducto(pedido1, producto2);

		Pedido pedido2 = new Pedido();
		pedido2.setInd(2);
		pedido2.setDestinatario("Paco Martín");
		pedido2.setDireccion("C/ Atalaya, 10");
		pedido2.setPeninsular(false);
		pedidoNoPeninsulaService.agregarProducto(pedido2, producto3);

		listaPedidos.add(pedido1);
		listaPedidos.add(pedido2);

		Scanner input = new Scanner(System.in);
		String op;

		// Se le muestra al usuario una serie de opciones que puede elegir
		// Al realizar una acción se vuelve a mostrar el menú hasta que decida cerrar la
		// aplicación (eligiendo la última opción)
		do {
			System.out.println("--------------------------------------------------------------");
			System.out.println("Seleccione una opción");
			System.out.println("1. Listar productos");
			System.out.println("2. Listar pedidos");
			System.out.println("3. Crear pedido");
			System.out.println("0. Salir");

			op = input.nextLine();
			int i = 0;

			switch (op) {
			case "1":
				System.out.println("--1. Listar productos--");
				for (Producto producto : listaProductos) {
					System.out.println(producto.toString());
				}
				System.out.println("");
				break;
			case "2":
				System.out.println("--2. Listar pedidos--");
				for (Pedido pedido : listaPedidos) {
					System.out.println(pedido.toString());
					i = 1;
					if (pedido.isPeninsular()) {
						for (Producto producto : pedido.getProductos()) {
							pedidoPeninsulaService.calcularPvpProducto(pedido, producto);
							System.out.println("\tProducto " + i + ": " + producto.getNombre() + ", " + producto.getPvp() + "€");
							i++;
						}
					} else if (!pedido.isPeninsular()) {
						for (Producto producto : pedido.getProductos()) {
							pedidoNoPeninsulaService.calcularPvpProducto(pedido, producto);
							System.out.println("\tProducto " + i + ": " + producto.getNombre() + ", " + producto.getPvp() + "€");
							i++;
						}
					}
				}
				System.out.println("");
				break;
			case "3":
				System.out.println("--3. Crear pedido--");
				Pedido pedido = new Pedido();
				List<Producto> productosComprar = new ArrayList<>();

				System.out.println("Introduzca el destinatario del pedido");
				String destinatario = input.nextLine();
				pedido.setDestinatario(destinatario);
				
				System.out.println("Introduzca el destino de envío");
				String direccion = input.nextLine();
				pedido.setDireccion(direccion);

				System.out.println("¿Es un envío a la Península? (sí/no)");
				String esPeninsular = input.nextLine();
				if(esPeninsular.toLowerCase().contains("s")) {
					pedido.setPeninsular(true);
				} else if(esPeninsular.toLowerCase().contains("n")) {
					pedido.setPeninsular(false);
				}
				
				pedido.setInd(listaPedidos.size() + 1);
				String op2;
				
				do {
					i = 1;
					for (Producto producto : listaProductos) {
						System.out.println("Producto " + i + ": " + producto.getNombre() + "," + producto.getPrecioSinImpuestos() + "€");
						i++;
					}
					System.out.println("Seleccione un producto (introduzca el número del producto)");
					int numProducto = Integer.parseInt(input.nextLine()) - 1;
					
					if(numProducto < listaProductos.size())
						productosComprar.add(listaProductos.get(numProducto));
					
					System.out.println("Quiere añadir otro producto (sí/no)");
					op2 = input.nextLine();
					
				} while(!op2.toLowerCase().contains("n"));
					for (Producto producto : productosComprar) {
						pedidoPeninsulaService.agregarProducto(pedido, producto);
					}
					
					listaPedidos.add(pedido);
				
				System.out.println("");
				break;
			case "0":
				System.out.println("Adiós");
				break;
			default:
				System.out.println("Opción no válida");
				break;
			}

		} while (!op.equalsIgnoreCase("0"));

		input.close();
	}

}
